
*** To compile:

mkdir build
cd build
cmake ..
make


*** To execute:

./src/main ../../../../instances/small/puboi_1000.json


*** Contact if any question:

Sebastien Verel, verel@univ-littoral.fr

*** Please cite this article if you use this code:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.
