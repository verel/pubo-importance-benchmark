/*
  walshEval.hpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.

Please cite this article if you use this code:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.

 */

#ifndef _WalshEval_h
#define _WalshEval_h

#include <iostream>
#include <fstream>
#include <vector>

#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"

/*
    Walsh function:
      x binary string,
          f(x) = \sum_k w_k phi_k(x)
          f(x) = \sum_k w_k (-1)^{\sum k_i * x_i}
            where x_i and k_i are the i binary digit of x and k
 */
class WalshEval {
public:
    WalshEval(std::string _filename) {
        std::ifstream file(_filename);
        if (file) {
            readJson(file);

            file.close();
        } else {
            std::cerr << "Impossible to open file " << _filename << std::endl;
        }
    }

    /*
        Compute the fitness value
     */
    double operator()(std::vector<bool> & x) const {
        double res = 0;

        for(unsigned k = 0; k < coefs.size(); k++) {
            // compute the parity of the number of 1 in the term k of x
            if (parity(x, k))
                res -= values[k];
            else
                res += values[k];
        }

        return res;
    }

    /*
        Print instance

        format=
        0: json
        1: txt
     */
    void print(unsigned int format = 0) const {
        printOn(std::cout, format);
    }

    // Length of the bit string (problem dimension)
    unsigned n;

    // Number of Walsh terms
    unsigned p;

    // Variables of each Walsh terms (vector (of size p) of variable lists)
    std::vector< std::vector<unsigned> > coefs;

    // Values for each coefficients/terms (vector of size p)
    std::vector< double > values;

protected:
    /*
        Parity of the number of 1 in x
            true  = 1 = odd  (-)
            false = 0 = even (+)

        Complexity O(k_max)
     */
    bool parity(std::vector<bool> & x, unsigned k) const {
        bool phi = false; // even

        for(unsigned c : coefs[k]) 
            phi = (phi ? !x[c] : x[c]); // xor

        return phi;
    }

    /*
              Read instance from in stream (json format)

              Format:
              {"problem": {"terms":[{"w": -2, "ids": [0, 1]},...]}}
              for each term:
              w_k w: val
              i_1 i_2 ... i_{n_{k}} ids:[vals]
     */
    void readJson(std::ifstream & ifs) {
        rapidjson::IStreamWrapper isw(ifs);

        rapidjson::Document d;
        d.ParseStream(isw);
        assert(d.IsObject());
        const rapidjson::Value& terms = d["problem"]["terms"];
        unsigned xi;
        n = 0;
        p = terms.Size();

        double w;
        for (int ind = 0; ind < p; ++ind) {
            std::vector<unsigned> coef;
            for (int j = 0; j < terms[ind]["ids"].Size(); ++j) {
                xi = terms[ind]["ids"][j].GetUint();
                if (xi > n)
                    n = xi;
                coef.push_back(xi);
            }
            coefs.push_back(coef);
            
            w = terms[ind]["w"].GetDouble();
            values.push_back(w);
        }
        n++;
    }

    virtual void printOn(std::ostream& _os, unsigned int format = 0) const {
        if (format == 0)
            printOnJson(_os);
        else
            printOnTxt(_os);
    }

    virtual void printOnTxt(std::ostream& _os) const {
        _os << n << " " << p << std::endl;

        for(unsigned k = 0; k < p; k++) {
            if (coefs[k].size() > 0)
                for(unsigned j : coefs[k])
                    _os << j << " " ;

            _os << values[k] ;

            _os << std::endl;
        }
    }

  void printOnJson(std::ostream & _os) const {
    _os << "{\"problem\":{" ;

    _os << "\"n\":" << n <<", ";

    // terms
    _os << "\"terms\":[";

    bool firstTerm = true;
    for(unsigned int i = 0; i < p; i++) {
      if (!firstTerm)
        _os << ",";
      else
        firstTerm = false;

      _os << "{\"w\":" << values[i] << ",";

      _os << "\"ids\":[" ;

      if (coefs[i].size() > 0)
        _os << coefs[i][0];
      
      for(unsigned int j = 1; j < coefs[i].size(); j++) 
        _os << "," << coefs[i][j];

      _os << "]" ; // end of ids

      _os << "}" ;  // end of term
    }

    _os << "]" ; // end of terms

    _os << "}}" << endl; // end of problems, and json file
  }

};

#endif
