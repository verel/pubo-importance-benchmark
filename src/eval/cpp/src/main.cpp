//-----------------------------------------------------------------------------
/** 
 *
 * Example of Walsh fitness function in c++
 *
 * 

Please cite this article if you use this code:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.

 */
//-----------------------------------------------------------------------------

// standard includes
#include <stdexcept>  
#include <iostream>   
#include <fstream>
#include <string.h>
#include <random>

// declaration of the namespace
using namespace std;

//-----------------------------------------------------------------------------
// fitness function
#include <walshEval.hpp>

// Main function
//-----------------------------------------------------------------------------
int main_function(int argc, char **argv)
{
    /* =========================================================
     *
     * Eval fitness function (full evaluation)
     *
     * ========================================================= */

    // the fitness function is the Walsh function
    if (argc != 2) {
        cerr << "Wrong number of parameters. Usage: ./src/main ../../../../instances/small/puboi_1000.json" << endl;
        return 0;
    }
    //argv[1] = instance file name // "../../../../instances/small/puboi_1000.json"

    string filename(argv[1]);

    WalshEval eval(filename);

    //cout << eval.n << endl;

    /* =========================================================
     *
     * executes from a random solution
     *
     * ========================================================= */

    // The solution
    vector<bool> solution(eval.n, true);

    // Evaluation of the initial solution:
    double fitness = eval(solution);

    // Output: the solution
    cout << fitness << " " ;
    for(auto b : solution)
        cout << b ;
    cout << endl;

    // Apply random initialization
    std::mt19937 rng(42);
    std::uniform_real_distribution<double> dist(0.0, 1.0);

    for(unsigned i = 0; i < solution.size(); i++)
        solution[i] = (dist(rng) < 0.5);

    fitness = eval(solution);

    // Output: the solution
    cout << fitness << " " ;
    for(auto b : solution)
        cout << b ;
    cout << endl;

    return 1;
}

// A main that catches the exceptions

int main(int argc, char **argv)
{
    try {
        main_function(argc, argv);
    }
    catch (exception& e) {
        cout << "Exception: " << e.what() << '\n';
    }
    return 1;
}
