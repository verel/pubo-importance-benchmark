#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
	Example of fitness function evaluation

	@authors: LEC group, LISIC Lab, Univ. of Littoral Opal Coast, France

Please cite this article if you use this code:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.

"""
import sys
import random

sys.path.insert(0, '../../generator')
from walsh_expansion import WalshExpansion

if __name__ == "__main__":
	# read an instance file
	f = WalshExpansion()
	f.load("../../../instances/small/puboi_1000.json")

	# Print to check the instance
	#print(f.n)
	#print(f)

	# basic solution
	x = [ 1 ] * f.n
	print(x)
	print(f.eval(x))

	# random solution
	for i in range(len(x)):
		if random.random() < 0.5:
			x[i] = -1
		else:
			x[i] = 1

	print(x)
	print(f.eval(x))
