#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Create a set of instances from the parameters given in a CVS file

Inputs:
- Input file name with parameters of instances
- Output file name with description of the corresponding instances
- Directory name of the instance files

Outputs
- File with description of instances
- Set of instances in the directory

See: 
puboi_param.csv and parameters_generator_evoCOP22.R for CSV format


Examples to execute this code from instance directory:
python ../src/generator/make_instances.py -I small/puboi_param_small.csv -O small/puboi_description_small.csv -D small
python ../src/generator/make_instances.py -I puboi_param.csv -O puboi_description.csv -D instancesEvoCOP

Becareful, the second example could be very long to execute...

Please cite this article if you use this code:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.

@authors: LEC group, LISIC Lab, Univ. of Littoral Opal Coast, France

"""
import argparse
import puboi_generator as wg
import pandas as pd

def make_instances():
	# arguments
	parser = argparse.ArgumentParser(description = 'Process input file name, output file name, and output directory name of instances.')
	parser.add_argument("-I", type = str, help = 'input file name with parameters of instances', default = "puboi_param.csv")
	parser.add_argument("-O", type = str, help = 'output file name with description of the corresponding instances', default = "puboi_description.csv")
	parser.add_argument("-D", type = str, help = 'output directory name for saving the instance files.', default = "../../instances")
	args = parser.parse_args()

	input_name   = args.I
	output_name  = args.O
	dir_instance = args.D

	# read file with main parameters of instances
	df = pd.read_csv(input_name, delimiter = ' ')

	# Output file of instance description: parameters header
	f = open(output_name, "w")

	f.write("id type n n_class size degree factor typeWeight n_p p m shift seed bound\n")

	# Classes of importance
	importance = { }

	factor = 1.0
	seed   = 0

	for idx, row in df.iterrows():
		id_instance = int(row['id'])

		n = int(row['n'])

		importance['size'] = [ int(s) for s in row['size'].split(",") ]
		importance['degree'] = [ float(di) for di in row['degree'].split(",") ]

		m = int(row['m'])

		p_function = [ float(pi) for pi in row['p'].split(',') ]

		factor = float(row['factor'])
		shift  = bool(row['shift'])
		seed   = int(row['seed'])

		# 4 functions in the portfolio (cf. )
		builder   = wg.PortfolioBuilder(n)
		portfolio = builder.make()

		# generator
		generator = wg.PUBOi_generator(m = m, importance = importance, factor = factor, portfolio = portfolio, \
			                           p_function = p_function, typeWeight = 0, shift = shift, seed = seed)
		
		# print parameters
		print_generator(id_instance, generator)

		# Create function
		W = generator.make()

		# print parameters into file
		write_generator(id_instance, generator, f)

		# export to json file
		filenameOut = ""
		if len(dir_instance) > 0:
			filenameOut = dir_instance + "/"
		filenameOut = filenameOut + "puboi_" + str(id_instance) + ".json"

		W.to_json(filenameOut, generator)

	f.close()

def print_generator(id_instance, generator):
	# print parameters
	print(str(id_instance), end='')
	print(" puboi", end='')
	print(" " + str(generator.n) + " %d \"%d" % (generator.n_class, generator.importance['size'][0]), end='')
	for i in range(1, generator.n_class):
		print(',' + str(generator.importance['size'][i]), end='')
	print('\" \"%.6f' % generator.importance['degree'][0], end='')
	for i in range(1, generator.n_class):
		print(',%.6f' % generator.importance['degree'][i], end='')
	print(('\" %.6f' % generator.factor) + ' ' + str(generator.typeWeight), end='')
	print(" 4 \"%.6f,%.6f,%.6f,%.6f\"" % (generator.p_function[0], generator.p_function[1], generator.p_function[2], generator.p_function[3]), end='')
	print(' ' + str(generator.m), end='')
	if generator.shift:
		print(' 1', end='')
	else:
		print(' 0', end='')
	print(' ' + str(generator.seed), end='')

	if hasattr(generator, 'bound'):
		print(' ' + str(generator.bound))
	else:
		print('')

def write_generator(id_instance, generator, f):
	# print parameters
	f.write(str(id_instance))
	f.write(" puboi")
	f.write(" " + str(generator.n) + " %d \"%d" % (generator.n_class, generator.importance['size'][0]))
	for i in range(1, generator.n_class):
		f.write(',' + str(generator.importance['size'][i]))
	f.write('\" \"%.6f' % generator.importance['degree'][0])
	for i in range(1, generator.n_class):
		f.write(',%.6f' % generator.importance['degree'][i])
	f.write(('\" %.6f' % generator.factor) + ' ' + str(generator.typeWeight))
	f.write(" 4 \"%.6f,%.6f,%.6f,%.6f\"" % (generator.p_function[0], generator.p_function[1], generator.p_function[2], generator.p_function[3]))
	f.write(' ' + str(generator.m))
	if generator.shift:
		f.write(' 1')
	else:
		f.write(' 0')
	f.write(' ' + str(generator.seed))

	if hasattr(generator, 'bound'):
		f.write(' ' + str(generator.bound))
	f.write('\n')

if __name__ == "__main__":
	make_instances()