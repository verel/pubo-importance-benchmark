# PUBOi: a tunable benchmark with variable importance

This benchmark is a set of instances from the problem: Polynomial Unconstrained Binary Optimization (PUBO) with variable importance.

More details are coming soon...

Please see article:
  Sara Tari, Sebastien Verel, and Mahmoud Omidvar. 
  "PUBOi: A Tunable Benchmark with Variable Importance." 
  In European Conference on Evolutionary Computation in Combinatorial Optimization (Part of EvoStar), pp. 175-190. Springer, Cham, 2022.


## Instance files

The directory 'instances' all instance files in json format.

The set of instances published in evoCOP 2022 is available in the tar.gz file "instancesEvoCOP.ta.gz".

## Generator

The generator is written in python.
Please see directory 'src/generator' to use the code.

## Evaluation function

Evaluation function are available in c++, and in python.

Please see directory 'src/eval'

## Contacts

Sébastien Verel, verel@univ-littoral.fr
Sara Tari, sara.tari@univ-littoral.fr
Mahmoud Omidvar, mahmoud.omidvar@univ-littoral.fr

